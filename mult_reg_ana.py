# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 15:22:42 2020

@author: Patrick
"""

# für Entwicklung: setze working-directory auf Speicherort der Datei
import os
os.chdir('D:/Dokumente/Studium/13. Semester/Bachelorarbeit/Daten')

###----------------------------------------------------------------------###
### importiere benötigte Module und definiere Funktionen
###----------------------------------------------------------------------###

# importiere benötigte Module
import numpy as np
import pandas as pd
import seaborn as sns
from scipy import stats
import matplotlib.pyplot as plt

import statsmodels.api as sm
from statsmodels.stats import diagnostic as diag
from statsmodels.stats.outliers_influence import variance_inflation_factor

from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error

import read_country

###----------------------------------------------------------------------###
### Load the Data into Pandas
###----------------------------------------------------------------------###

# Daten einlesen - wahlweise standardisiert ('Dateiname_stand') oder nicht
# wahlweise mit allen Ländern (außer Schengen) ('Dateiname') oder ohne visafreie ('Dateiname_visafrei')
# zur Reproduktion der Ergebnisse der Bachelorarbeit, verwende als Quelle
# 'verwendete_daten.csv'
#visa_df = pd.read_csv('daten_mult_reg.csv')
visa_df = pd.read_csv('daten_mult_reg.csv')
# Länderspalte als Index setzen
visa_df = visa_df.set_index('Code')
# gehe sicher, das alle Elemente Zahlen sind
visa_df = visa_df.astype(float)

# Schengen-Länder oder visafreie Länder rauslassen
print('Schengen-Länder werden eingelesen')
# wähle aus, laender_drop = 'schengen' oder 'visafrei'
laender_drop = 'schengen'

if laender_drop == 'schengen':
    # schengen-Länder oder alle visabefreiten Länder rauslassen
    # schengen = pd.read_excel('Laender_visafrei_2019.xlsx')
    schengen = pd.read_excel('Laender_Schengen.xlsx')
    schengen = read_country.set_land_index(schengen, 'Land')
    # um Error zu vermeiden, nur die Schengenländer auswählen, für die tatsächlich Daten vorliegen
    # schengen = set(schengen.index) & set(visa_ablehnungsquote.index)
    # entsprechende Zeilen löschen
    visa_df = visa_df.drop(schengen.index)
elif laender_drop == 'visafrei':
    visafrei = pd.read_excel('Laender_visafrei_2019.xlsx')
    visafrei = read_country.set_land_index(visafrei, 'Land')
    visa_df = visa_df.drop(visafrei.index)
else:
    pass
    
# Lasse Länder weg, in denen keine Auslandsvertretung besteht
visa_df = visa_df[visa_df['visa_ablehnungsquote'].notna()]

'''
# check for nulls nach Spalten (Variablen)
miss_variable = visa_df.isnull().any()
print('vollständige Variablensätze = False')
display(miss_variable.value_counts())
# check for nulls nach Reihen (Länder)
miss_country = visa_df.T.isnull().any()
print('vollständige Länderdatensätze = False')
display(miss_country.value_counts())
'''

# workaround for missing data: Länder mit fehlenden Daten einfach weglassen
visa_df_before = visa_df.dropna()
# transferiert in daten_einlesen.py --> sollte jetzt unnötig sein
# erstelle neuen Faktor: schutzsuchende_rel
#visa_df['schutzsuchende_rel'] = visa_df['schutzsuchende_gesamt']/visa_df['auslaender_gesamt']
transform = True

if transform:
    # manche Daten Transformieren
    # power Logarithmieren
    visa_df['power'] = visa_df['power'].apply(np.log)
    visa_df['terror_kill'] = visa_df['terror_kill'].apply(lambda x: np.log(x+1))
    #visa_df['terror_kill'] = visa_df['terror_kill'].apply(lambda x: x**0.5)
    visa_df['col_ind_date'] = visa_df['col_ind_date'].apply(lambda x: np.log(2020-x))
    # Ausreiser außerhalb 2 Standardabweichungen beim Unabhängigkeitsdatum auf den Randwert setzen
    #visa_df['col_ind_date'][visa_df['col_ind_date'] < ( visa_df['col_ind_date'].mean() - (visa_df['col_ind_date'].std() * 2))] = ( visa_df['col_ind_date'].mean() - (visa_df['col_ind_date'].std() * 2)) 
    # visa_df['col_ind_date'][visa_df['col_ind_date'] < 1900] = 1900 
    # Ablehnungsquote wurzeln, da die Quote quasi poissonverteilt ist
    #visa_df['visa_ablehnungsquote'] = visa_df['visa_ablehnungsquote'].apply(lambda x: x**0.25)
    visa_df['visa_ablehnungsquote'] = visa_df['visa_ablehnungsquote'].apply(np.log)
    #visa_df['schutzsuchende_rel'] = visa_df['schutzsuchende_rel'].apply(lambda x: x**0.5)
    #visa_df['rel_krim_gesamt_ohne_aufenthalt'] = visa_df['rel_krim_gesamt_ohne_aufenthalt'].apply(lambda x: x**0.5)
    visa_df['schutzsuchende_rel'] = visa_df['schutzsuchende_rel'].apply(lambda x: np.log(x+0.00000001))
    visa_df['rel_krim_gesamt_ohne_aufenthalt'] = visa_df['rel_krim_gesamt_ohne_aufenthalt'].apply(np.log)
    #visa_df['polity_v_polity'] = visa_df['polity_v_polity'].apply(np.abs)
else:
    visa_df['log_gni_ppp'] = visa_df['log_gni_ppp'].apply(np.exp)


###----------------------------------------------------------------------###
### Check for perfect multicollinearity
###----------------------------------------------------------------------###

# calculate the correlation matrix
#corr = visa_df_before.corr()

# display the correlation matrix
# display(corr)

# plot the correlation heatmap
#sns.heatmap(corr, xticklabels=corr.columns, yticklabels=corr.columns, cmap='RdBu')

# drop correlated data
# data to keep: visa_ablehnungsquote, col_deutsch, col_ind_date, auslaender_gesamt,
# schutzsuchende_gesamt, schutzsuchende_rel, world_happiness_dem_quality
# erwerbslosenquote, hdi
#
# bisher signifikant: hdi, rel_krim_gesamt_ohne_aufenthalt
#
# erwerbslosenquote nicht signifikant. Vielleicht erwerbsquote? --> dann ist
# cond number large, d.h. multicollinearity
#
# unsicher: world_happiness_perc_corruption
# welche rel_krim-Faktoren?
list_to_drop = ['auslaender_mannlich', 'auslaender_weiblich', 'bws_dienst',
                'bws_land', 'bws_prod', 'krim_gesamt_ohne_aufenthalt',
                'erwerbsquote', 'krim_gewalt', 'krim_rohheit', 'krim_sex',
                'krim_gesamt', 'krim_leben', 'krim_wirtschaft',
                'opf_stat_gesamt', 'schutzsuchende_maennlich',
                'schutzsuchende_weiblich', 'world_happiness_confidence_nat_gover',
                'world_happiness_deliv_quality', 'world_happiness_freedom_choices',
                'world_happiness_generosity', 'world_happiness_life_expectancy',
                'world_happiness_life_ladder', 'world_happiness_social_support',
                'rel_krim_gewalt', 'rel_krim_rohheit', 'rel_krim_sex', 
                'rel_krim_gesamt', 'rel_krim_leben', 'rel_krim_wirtschaft', 
                'rel_opf_stat_gesamt', 'unabhaengigkeit', 'region', 
                'Latin American', 'Sinic', 'Japanese', 'Islamic', 'Hindu', 
                'Buddhist', 'African', 'Western', 'Ostasien', 'Südostasien', 
                'Lateinamerika und Karibik', 'Postsowjetischer Raum', 
                'Orthodox', 'auslaender_gesamt', 'non_self_governing_year',
                'terror_wound', 'terror_number', 'world_happiness_dem_quality', 
                'polity_v_autoc', 'polity_v_democ', 'freedom_house_civil', 
                'freedom_house_political', 'polity_v_polity', #'freedom_house_both',
                'auslaender_gesamt', 'col_deutsch', 
                'world_happiness_perc_corruption', 
                'schutzsuchende_gesamt', 'passport_power', 'civilization',
                'erwerbslosenquote', 'bip_pers', 'power_rank', 'hdi',
                'world_happiness_log_gdp_capita',
                'postkol', 'erwerb', 'erwerb_quotient', 'Südasien',
                #'col_ind_date', 'non_self_governing',
                #'power'
                # AIC-optimiert für visafrei
                #'non_self_governing', 'Mena', 'power', 'schutzsuchende_rel', 'freedom_house_both', 'Subsahara-Afrika'
                # AIC-optimiert für Schengen
                #'schutzsuchende_rel', 'Mena', 'power', 'non_self_governing', 'col_ind_date', 'Subsahara-Afrika', 
                ]

# speichere die Liste für die anderen Programme
np.save('list_to_drop', list_to_drop)

# define dataframes before and after dropping of correlated variables
# drop missing values in both dataframes
#visa_df_before = visa_df


# lasse unnötige Variablen weg
visa_df_after = visa_df.drop(list_to_drop, axis = 1)

# check for nulls nach Spalten (Variablen)
miss_variable = visa_df_after.isnull().any()
print('vollständige Variablensätze = False')
display(miss_variable.value_counts())
# check for nulls nach Reihen (Länder)
miss_country = visa_df_after.T.isnull().any()
print('vollständige Länderdatensätze = False')
display(miss_country.value_counts())

# lasse jetzt missing data weg
visa_df_after = visa_df_after.dropna()

# alle Extremwerte auf 2-sigma-Intervall setzen

for key in visa_df_after.keys():
    # verwendete dummy-Variablen nicht konvertieren
    if key not in ['Mena', 'Subsahara-Afrika', 'Südasien']:
        # unterer Wert
        visa_df_after[key][visa_df_after[key] < ( visa_df_after[key].mean() - (visa_df_after[key].std() * 2))] = ( visa_df_after[key].mean() - (visa_df_after[key].std() * 2)) 
        # oberer Wert
        visa_df_after[key][visa_df_after[key] > ( visa_df_after[key].mean() + (visa_df_after[key].std() * 2))] = ( visa_df_after[key].mean() + (visa_df_after[key].std() * 2)) 


# droppe nicht signifikante Werte
#list_to_drop = ['Subsahara-Afrika', 'non_self_governing', 'polity_v_polity', 'schutzsuchende_rel', 'power']
#visa_df_after = visa_df_after.drop(list_to_drop, axis = 1)
#visa_df_after = visa_df_after.dropna()


# correlations after dropping of correlated data
corr_after = visa_df_after.corr()
plot_corr_after = sns.heatmap(corr_after, xticklabels=corr_after.columns, yticklabels=corr_after.columns, cmap='RdBu')
#'''

# the VFI does expect a constant term in the data, so we need to add one using the add_constant method
#X1 = sm.tools.add_constant(visa_df_before)
X2 = sm.tools.add_constant(visa_df_after)

# create the series for both
#series_before = pd.Series([variance_inflation_factor(X1.values, i) for i in range(X1.shape[1])], index=X1.columns)
try:
    series_after = pd.Series([variance_inflation_factor(X2.values, i) for i in range(X2.shape[1])], index=X2.columns)
except:
    series_after = ['ERROR']
# display the series
#print('DATA BEFORE')
#print('-'*100)
#display(series_before)

print('DATA AFTER')
print('-'*100)
display(series_after)

# show scatter plot for each variable
# define the plot
pd.plotting.scatter_matrix(visa_df_after, alpha = 1, figsize = (30, 20))

# show the plot
plt.show()


###----------------------------------------------------------------------###
### Describe the Data Set
###----------------------------------------------------------------------###

# get the summary
desc_df = visa_df_after.describe()

# add the standard deviation metric
desc_df.loc['+3_std'] = desc_df.loc['mean'] + (desc_df.loc['std'] * 3)
desc_df.loc['-3_std'] = desc_df.loc['mean'] - (desc_df.loc['std'] * 3)

# display it
display(desc_df)


###----------------------------------------------------------------------###
### Build the Model
###----------------------------------------------------------------------###

# define our input variable (X) & output variable
X = visa_df_after.drop('visa_ablehnungsquote', axis = 1)
Y = visa_df_after[['visa_ablehnungsquote']]

# Split X and y into X_
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.20, random_state=1)

# create a Linear Regression model object
regression_model = LinearRegression()

# pass through the X_train & y_train data set
regression_model.fit(X_train, y_train)

# exploring the output
# let's grab the coefficient of our model and the intercept
intercept = regression_model.intercept_[0]
coefficent = regression_model.coef_[0][0]

print("The intercept for our model is {:.4}".format(intercept))
print('-'*100)

# loop through the dictionary and print the data
for coef in zip(X.columns, regression_model.coef_[0]):
    print("The Coefficient for {} is {:.2}".format(coef[0],coef[1]))

# Get multiple predictions
y_predict = regression_model.predict(X_test)

# Show the first 5 predictions
y_predict[:5]

###----------------------------------------------------------------------###
### Evaluating the Model
###----------------------------------------------------------------------###

# define our input
X2 = sm.add_constant(X)

# create a OLS model
model = sm.OLS(Y, X2)
# try weighted least squares model wegen heterodescedasticity
#model = sm.WLS(Y, X2)


# fit the data
est = model.fit()


###
### Check for heteroscedasticity
###

# Run the White's test
try:
    _, pval, __, f_pval = diag.het_white(est.resid, est.model.exog, retres = False)
    print(pval, f_pval)
    print('-'*100)


    # print the results of the test
    if pval > 0.05:
        print("For the White's Test")
        print("The p-value was {:.4}".format(pval))
        print("We fail to reject the null hypthoesis, so there is no heterosecdasticity. \n")
        
    else:
        print("For the White's Test")
        print("The p-value was {:.4}".format(pval))
        print("We reject the null hypthoesis, so there is heterosecdasticity. \n")

except:
    print("Fehler in White's Test")
    

# Run the Breusch-Pagan test
_, pval, __, f_pval = diag.het_breuschpagan(est.resid, est.model.exog)
print(pval, f_pval)
print('-'*100)

# print the results of the test
if pval > 0.05:
    print("For the Breusch-Pagan's Test")
    print("The p-value was {:.4}".format(pval))
    print("We fail to reject the null hypthoesis, so there is no heterosecdasticity.")

else:
    print("For the Breusch-Pagan's Test")
    print("The p-value was {:.4}".format(pval))
    print("We reject the null hypthoesis, so there is heterosecdasticity.")



###
### Check for Autocorrelation
### 

# test for autocorrelation
from statsmodels.stats.stattools import durbin_watson

# calculate the lag, optional
lag = min(10, (len(X)//5))
print('The number of lags will be {}'.format(lag))
print('-'*100)

# run the Ljung-Box test for no autocorrelation of residuals
# test_results = diag.acorr_breusch_godfrey(est, nlags = lag, store = True)
test_results = diag.acorr_ljungbox(est.resid, lags = lag)

# grab the p-values and the test statistics
ibvalue, p_val = test_results

# print the results of the test
if min(p_val) > 0.05:
    print("The lowest p-value found was {:.4}".format(min(p_val)))
    print("We fail to reject the null hypthoesis, so there is no autocorrelation.")
    print('-'*100)
else:
    print("The lowest p-value found was {:.4}".format(min(p_val)))
    print("We reject the null hypthoesis, so there is autocorrelation.")
    print('-'*100)

# plot autocorrelation
sm.graphics.tsa.plot_acf(est.resid)
plt.show()

###
### Checking for Normally Distributed Residuals
### checking the meand of the residuals equals 0
###

import pylab

# check for the normality of the residuals
sm.qqplot(est.resid, line='s')
pylab.show()

# also check that the mean of the residuals is approx. 0.
mean_residuals = sum(est.resid)/ len(est.resid)
print("The mean of the residuals is {:.4}".format(mean_residuals))


###
### Measures of Error
###


import math
# calculate the mean squared error
model_mse = mean_squared_error(y_test, y_predict)

# calculate the mean absolute error
model_mae = mean_absolute_error(y_test, y_predict)

# calulcate the root mean squared error
model_rmse =  math.sqrt(model_mse)

# display the output
print("MSE {:.3}".format(model_mse))
print("MAE {:.3}".format(model_mae))
print("RMSE {:.3}".format(model_rmse))

# R-Squared
model_r2 = r2_score(y_test, y_predict)
print("R2: {:.2}".format(model_r2))


# make some confidence intervals, 95% by default
confidence_intervals = est.conf_int()

# Hypothesis Testing, get p-values
# estimate the p-values
p_values = est.pvalues

# print out a summary
print(est.summary())
print(est.summary().as_latex())

'''
#probiere anderes Modell aus, GLM, WLS, OLS
model = sm.GLM(Y, X2)
#model = sm.GLSAR(Y, X2)
model = sm.RLM(Y, X2)
model = sm.Logit(Y, X2)
# fit the data
est = model.fit()
# print out a summary
print(est.summary())
'''




