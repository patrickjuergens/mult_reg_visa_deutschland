import pandas as pd

def find_country_code(land, laender):
    #Leerzeilen in Ländernamen löschen
    land = str(land).replace('\n',' ')
    #Ländername in Länderliste suchen und Index zurückgeben
    for key in laender.keys():
        # print('Key ist ' + key)
        # checke als erstes, ob die ignore-Liste existiert
        if 'laender_ignore_df' not in globals():
            global laender_ignore_df
            laender_ignore_df = pd.read_excel('Laender_ignore.xlsx')
        # Die Einträge für Gesamtsumme der Länder in 'XXX' speichern, 
        # also ignorieren
        # ebenso wenn Eintrag 'nan' ist oder in ignorier-Liste
        if not isinstance(land, str):
            #print('1. if')
            return len(list(laender[key])) - 1
        elif 'Gesamt' in land or land in laender_ignore_df.iloc[:,0].unique():
            #print('2. if')
            return len(list(laender[key])) - 1
        elif land == 'nan':
            return len(list(laender[key])) - 1
        #elif land in laender_ignore_df.iloc[:,0].unique():
        #    return len(list(laender[key])) - 1
        elif land in list(laender[key]):
            #print('3. if')
            return list(laender[key]).index(land)
    print('Land "' + land + '" nicht in der Datenbank')
    print(type(land))
    # das Land zur Länder-ignore-Liste hinzufügen, damit es in Zukunft gleich gecheckt wird, dass es fehlt
    laender_ignore_df = laender_ignore_df.set_value(len(laender_ignore_df), 'XXX', land)
    # Wenn das Land nicht gefunden wurde, gebe letzten Index der Datenbank
    # aus --> Sammlung für nicht gefundene Ländereinträge
    return len(list(laender[key])) - 1

# Funktion, um Indizes der Länder im Dataframe zu ändern.
# benutzt find_country_code von oben
def set_land_index(dataframe, laender_column_label, laender_dataframe = 'laender_df'):
    if laender_dataframe == 'laender_df':
        if 'laender_df' not in globals():
            global laender_df
            laender_df = pd.read_excel('Laenderliste.xlsx')
            laender_df = laender_df.set_index('Code', drop = False)
        laender_dataframe = laender_df
    # suche Liste mit den Ländernamen
    laender_column = dataframe[laender_column_label]
    # gleiche Ländernamen mit Länderliste ab und finde Indizes
    laender_indices = [find_country_code(laender_column[i], laender_dataframe) for i in range(len(laender_column))]
    # ersetze numerische Indizes durch Ländercodes
    laender_indices = laender_dataframe.index[laender_indices]
    # Schreibe Länderindizes in das dataframe
    dataframe = dataframe.set_index(laender_indices)
    # Lösche Platzhalterzeile mit Ländercode 'XXX', falls vorhanden
    try:
        dataframe = dataframe.drop('XXX')
    except:
        pass
    return dataframe
