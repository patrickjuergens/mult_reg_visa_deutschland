# -*- coding: utf-8 -*-
"""
Created on Thu Feb  6 10:26:38 2020

@author: Patrick
"""

# für Entwicklung: setze working-directory auf Speicherort der Datei
import os
os.chdir('D:/Dokumente/Studium/13. Semester/Bachelorarbeit/Daten')

###----------------------------------------------------------------------###
### importiere benötigte Module und definiere Funktionen
###----------------------------------------------------------------------###

# importiere benötigte Module
import numpy as np
import pandas as pd
import seaborn as sns
from scipy import stats
import matplotlib.pyplot as plt

import statsmodels.api as sm
from statsmodels.stats import diagnostic as diag
from statsmodels.stats.outliers_influence import variance_inflation_factor

from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error

# Funktion, um Indizes der Länder zu finden
def find_country_code(land, laender):
    #Leerzeilen in Ländernamen löschen
    land = str(land).replace('\n',' ')
    #Ländername in Länderliste suchen und Index zurückgeben
    for key in laender.keys():
        # print('Key ist ' + key)
        # checke als erstes, ob die ignore-Liste existiert
        if 'laender_ignore_df' not in globals():
            global laender_ignore_df
            laender_ignore_df = pd.read_excel('Laender_ignore.xlsx')
        # Die Einträge für Gesamtsumme der Länder in 'XXX' speichern, 
        # also ignorieren
        # ebenso wenn Eintrag 'nan' ist oder in ignorier-Liste
        if not isinstance(land, str):
            #print('1. if')
            return len(list(laender[key])) - 1
        elif 'Gesamt' in land or land in laender_ignore_df.iloc[:,0].unique():
            #print('2. if')
            return len(list(laender[key])) - 1
        elif land == 'nan':
            return len(list(laender[key])) - 1
        #elif land in laender_ignore_df.iloc[:,0].unique():
        #    return len(list(laender[key])) - 1
        elif land in list(laender[key]):
            #print('3. if')
            return list(laender[key]).index(land)
    print('Land "' + land + '" nicht in der Datenbank')
    print(type(land))
    # das Land zur Länder-ignore-Liste hinzufügen, damit es in Zukunft gleich gecheckt wird, dass es fehlt
    laender_ignore_df = laender_ignore_df.set_value(len(laender_ignore_df), 'XXX', land)
    # Wenn das Land nicht gefunden wurde, gebe letzten Index der Datenbank
    # aus --> Sammlung für nicht gefundene Ländereinträge
    return len(list(laender[key])) - 1

# Funktion, um Indizes der Länder im Dataframe zu ändern.
# benutzt find_country_code von oben
def set_land_index(dataframe, laender_column_label, laender_dataframe):
    # suche Liste mit den Ländernamen
    laender_column = dataframe[laender_column_label]
    # gleiche Ländernamen mit Länderliste ab und finde Indizes
    laender_indices = [find_country_code(laender_column[i], laender_dataframe) for i in range(len(laender_column))]
    # ersetze numerische Indizes durch Ländercodes
    laender_indices = laender_dataframe.index[laender_indices]
    # Schreibe Länderindizes in das dataframe
    dataframe = dataframe.set_index(laender_indices)
    # Lösche Platzhalterzeile mit Ländercode 'XXX', falls vorhanden
    try:
        dataframe = dataframe.drop('XXX')
    except:
        pass
    return dataframe


def standardize(df, label):
    """
    standardizes a series with name ``label'' within the pd.DataFrame
    ``df''.
    """
    df = df.copy(deep=True)
    series = df.loc[:, label]
    avg = series.mean()
    stdv = series.std()
    series_standardized = (series - avg)/ stdv
    return series_standardized

###----------------------------------------------------------------------###
### eigentliches Programm beginnt hier
###----------------------------------------------------------------------###

###----------------------------------------------------------------------###
### Lade allgemein benutzte Daten - Länderliste und Co
###----------------------------------------------------------------------###

print('Länderliste wird eingelesen')

# Lade liste mit Ländercodes und Ländernamen
laender_df = pd.read_excel('Laenderliste.xlsx')

# setze als Index den ISO-Ländercode
laender_df = laender_df.set_index('Code', drop = False)

#laender_ignore_df = pd.read_excel('Laender_ignore.xlsx')

# initialisiere dictionary, in dem die dataframes gespeichert werden
dict_dataframes = {}

###----------------------------------------------------------------------###
### Visa-Daten einlesen
###----------------------------------------------------------------------###

print('Visadaten werden eingelesen')


# Zahlen für erteilte und abgelehnte Visa einlesen

# erstelle leeres Datenframe mit Länder-Indizes für abgelehnte und bewilligte Visa
df_visa_abgelehnt = pd.DataFrame(index = laender_df.index)
df_visa_erteilt = pd.DataFrame(index = laender_df.index)

# Struktur der Excel-Tabellen
# Jahre 2010 bis 2017 (inklusive)
# Name excel-Datei: JJJJ_Visaerteilungen
# Land - AV-Ort - Visadaten
# Name der relevanten Spalten: Gesamt erteilt JJJJ, Gesamt abgelehnt JJJJ
# für 2007-2009 (und 2000)
# Name der Datei: 2007_2009_Visaerteilungen
# Name der Spalten: JJJJ abgelehnt, JJJJ erteilt

for jahr in range(2007, 2018):
    
    # Daten einlesen aus Excel-Tabellen
    if jahr < 2007 or jahr > 2017:
        print('keine Daten für Jahr ' + str(jahr))
        continue
    elif jahr < 2010:
        visa_df = pd.read_excel('Visaerteilungen/2007-2009_Visaerteilungen.xlsx')
    elif jahr >= 2010:
        visa_df = pd.read_excel('Visaerteilungen/' + str(jahr) + '_Visaerteilungen.xlsx')
    print('Daten für ' + str(jahr) + ' wurden eingelesen.')
    
    # setze Länder-Code als Indizes für die Spalten
    visa_df = set_land_index(visa_df, 'Land', laender_df)
    
    # addiere Zahlen aus gleichen Ländern
    # wähle die zu addierenden Spalten
    keys_visa = visa_df.keys()[2:]
    #konvertiere Einträge in Spalten in Integers
    visa_df[keys_visa] = visa_df[keys_visa].apply(pd.to_numeric, errors = 'coerce')
    # addiere jetzt die Zeilen
    visa_df = visa_df.groupby(visa_df.index)[keys_visa].sum()
    
    # schreibe die Zahlen für abgelehnte und erteilte Visa in die Datenbanken
    if jahr < 2010:
        df_visa_abgelehnt[str(jahr)] = visa_df[str(jahr) + ' abgelehnt']
        df_visa_erteilt[str(jahr)] = visa_df[str(jahr) + ' erteilt']
    elif jahr >= 2010:
        df_visa_abgelehnt[str(jahr)] = visa_df['Gesamt abgelehnt ' + str(jahr)]
        df_visa_erteilt[str(jahr)] = visa_df['Gesamt erteilt ' + str(jahr)]

# store variable in dictionary
dict_dataframes['visa_abgelehnt'], dict_dataframes['visa_erteilt'] = df_visa_abgelehnt, df_visa_erteilt


# nicht mehr verwendete Variablen löschen
try:
    del jahr, keys_visa, visa_df, df_visa_abgelehnt, df_visa_erteilt

except:
    pass


###----------------------------------------------------------------------###
### Ausländerstatistik einlesen - Funktion
### Gesamtzahl Ausländer sowie Schutzsuchende einlesen
###----------------------------------------------------------------------###
    
print('Ausländerstatistik wird eingelesen')


def einlesen_auslaender_statistik(dateiname):
    # Ausländerstatistik einlesen
    # beachte: Es gibt Zeilen für männlich, weiblich und gesamt!
    auslaender = pd.read_excel(dateiname, header = 5, index_col = None)
    #Setze Länder in die Spalte 'index'
    auslaender = auslaender.reset_index()
    # benenne diese Zeile in 'Land'
    auslaender.rename(columns = {'index':'Land'}, inplace = True)
    
    # benenne die Zeilen um
    # bekomme Zeilennamen
    list_column_names_old = list(auslaender.columns.values)
    # lösche '31.12.' aus Zeilennamen
    list_column_names_new = [old_name.replace('31.12.',"") for old_name in list_column_names_old]
    # benenne Zeilennamen neu
    auslaender.columns = list_column_names_new
    
    # finde Indizes, wo 'davon' steht, d.h. wo männlich, weiblich, gesamt beginnt
    ausl_ind = auslaender[auslaender['Land'] == 'davon:'].index.tolist()
    # Splitte df_auslaender in 4 dataframes, [müll, männlich, weiblich, gesamt]
    auslaender = np.split(auslaender, ausl_ind, axis = 0)
    
    # Setze die Länder-Namen als neue Indizes und bereinige Datenbanken
    # iteriere über die 4 dataframes
    for index, df in enumerate(auslaender):
        df = df.reset_index(drop = True)
        # Setze neuen Index
        loop_df = set_land_index(df, 'Land', laender_df)
        # addiere Zahlen aus gleichen Ländern
        # wähle die zu addierenden Zeilen
        keys_loop = loop_df.keys()[1:]
        #konvertiere Einträge in Spalten in Integers
        loop_df[keys_loop] = loop_df[keys_loop].apply(pd.to_numeric, errors = 'coerce')
        # addiere jetzt die Zeilen
        loop_df = loop_df.groupby(loop_df.index)[keys_loop].sum()
        
        auslaender[index] = loop_df
    
    # return folgende Form: [_, df_auslaender_maennlich, df_auslaender_weiblich, df_auslaender_gesamt]
    return auslaender

#[_, df_auslaender_maennlich, df_auslaender_weiblich, df_auslaender_gesamt] = einlesen_auslaender_statistik('Indizes/Ausländerstatistik_gesamt.xlsx')
#[_, df_schutzsuchende_maennlich, df_schutzsuchende_weiblich, df_schutzsuchende_gesamt] = einlesen_auslaender_statistik('Indizes/Ausländerstatistik_Schutzsuchende.xlsx')

# store variable in dictionary
[_, dict_dataframes['auslaender_mannlich'], dict_dataframes['auslaender_weiblich'], dict_dataframes['auslaender_gesamt']] = einlesen_auslaender_statistik('Indizes/Ausländerstatistik_gesamt.xlsx')
[_, dict_dataframes['schutzsuchende_maennlich'], dict_dataframes['schutzsuchende_weiblich'], dict_dataframes['schutzsuchende_gesamt']] = einlesen_auslaender_statistik('Indizes/Ausländerstatistik_Schutzsuchende.xlsx')


###----------------------------------------------------------------------###
### World Happiness Report einlesen
### life ladder, log GDP per capita, social support, life expectancy,
### freedom of choices, generosity, perception of corruption,
### confidence in national government, democratic quality, delivery quality
###----------------------------------------------------------------------###

print('World Happiness Report wird eingelesen')


# Excel-Tabelle einlesen
world_happiness = pd.read_excel('Indizes/Chapter2OnlineData.xls')
# Index in Ländercodes umändern
world_happiness = set_land_index(world_happiness, 'Country name', laender_df)

# Dataframes initialisieren
dict_dataframes['world_happiness_life_ladder'] = pd.DataFrame(index = world_happiness.index.unique())
dict_dataframes['world_happiness_log_gdp_capita'] = pd.DataFrame(index = world_happiness.index.unique())
dict_dataframes['world_happiness_social_support'] = pd.DataFrame(index = world_happiness.index.unique())
dict_dataframes['world_happiness_life_expectancy'] = pd.DataFrame(index = world_happiness.index.unique())
dict_dataframes['world_happiness_freedom_choices'] = pd.DataFrame(index = world_happiness.index.unique())
dict_dataframes['world_happiness_generosity'] = pd.DataFrame(index = world_happiness.index.unique())
dict_dataframes['world_happiness_perc_corruption'] = pd.DataFrame(index = world_happiness.index.unique())
dict_dataframes['world_happiness_confidence_nat_gover'] = pd.DataFrame(index = world_happiness.index.unique())
dict_dataframes['world_happiness_dem_quality'] = pd.DataFrame(index = world_happiness.index.unique())
dict_dataframes['world_happiness_deliv_quality'] = pd.DataFrame(index = world_happiness.index.unique())

# Datenbank aufsplitten nach Jahren, 2005-2018
for year in range(2005,2019):
    # Datenbanken für Daten aus world_happiness-Report erstellen
    # in der gleichen Form wie bisher, pro Faktor eine Datenbank
    # in den Spalten jeweils die Daten pro Jahr
    dict_dataframes['world_happiness_life_ladder'][str(year)] = world_happiness[world_happiness['Year'] == year]['Life Ladder']
    dict_dataframes['world_happiness_log_gdp_capita'][str(year)] = world_happiness[world_happiness['Year'] == year]['Log GDP per capita']
    dict_dataframes['world_happiness_social_support'][str(year)] = world_happiness[world_happiness['Year'] == year]['Social support']
    dict_dataframes['world_happiness_life_expectancy'][str(year)] = world_happiness[world_happiness['Year'] == year]['Healthy life expectancy at birth']
    dict_dataframes['world_happiness_freedom_choices'][str(year)] = world_happiness[world_happiness['Year'] == year]['Freedom to make life choices']
    dict_dataframes['world_happiness_generosity'][str(year)] = world_happiness[world_happiness['Year'] == year]['Generosity']
    dict_dataframes['world_happiness_perc_corruption'][str(year)] = world_happiness[world_happiness['Year'] == year]['Perceptions of corruption']
    dict_dataframes['world_happiness_confidence_nat_gover'][str(year)] = world_happiness[world_happiness['Year'] == year]['Confidence in national government']
    dict_dataframes['world_happiness_dem_quality'][str(year)] = world_happiness[world_happiness['Year'] == year]['Democratic Quality']
    dict_dataframes['world_happiness_deliv_quality'][str(year)] = world_happiness[world_happiness['Year'] == year]['Delivery Quality']
   
# nicht mehr verwendete Variablen löschen
try:
    del year, world_happiness
except:
    pass
    
###----------------------------------------------------------------------###
### Daten für Kolonialgeschichte einlesen
###----------------------------------------------------------------------###    

print('Kolonialgeschichte wird eingelesen')


# Datenbank einlesen    
kol_geschichte = pd.read_csv('Indizes/coldata110.csv')
# Index in Ländercodes umändern
kol_geschichte = set_land_index(kol_geschichte, 'Name', laender_df)

# Unabhängigkeitsdatum - Jahr in dataframe speichern
df_col_ind_date = kol_geschichte['IndDate']//100
df_col_ind_date = df_col_ind_date.groupby(df_col_ind_date.index).min()

# deutsche Kolonialgeschichte. ehemalige deutsche Kolonie: 1, nicht deutsche Kolonie: 0
df_col_deutsch = pd.DataFrame(index = laender_df.index)
# alles auf 0 setzen
df_col_deutsch['colDeutsch'] = 0
# ehemalige deutsche Kolonien auf 1 setzen
# Namibia, Togo, Kamerun, Tansania, Ruanda, Burundi, Papua-Neuguinea, 
# Marshallinseln, Salomonen, Nauru, Palau, Mikronesien, Samoa
# 'NAM', 'TGO', 'CMR', 'TZA', 'RWA', 'BDI', 'PNG', 'MHL', 'SLB', 'NRU', 'PLW', 'FSM', 'WSM'
df_col_deutsch['colDeutsch']['NAM', 'TGO', 'CMR', 'TZA', 'RWA', 'BDI', 'PNG', 'MHL', 'SLB', 'NRU', 'PLW', 'FSM', 'WSM'] = 1


# variablen in dictionary speichern
dict_dataframes['col_ind_date'] = df_col_ind_date
dict_dataframes['col_deutsch'] = df_col_deutsch

# nicht mehr verwendete Variablen löschen
try:
    del kol_geschichte, df_col_ind_date, df_col_deutsch
except:
    pass



###----------------------------------------------------------------------###
### Daten statistisches Bundesamt
###----------------------------------------------------------------------### 

print('Daten statistisches Bundesamt werden eingelesen')



def einlesen_stat_bundesamt(dateiname, spaltenname_laender):
    ''' liest Daten aus den Tabellen des statistischen Bundesamt ein
    gibt eine Liste von dataframes zurück, [1. Faktor, 2. Faktor, ...]'''
    # Daten Erwerbsquote einlesen
    stat_bundesamt = pd.read_excel(dateiname, header = 2)
    # ermittle Länge der Datenblöcke, indem Abstand zwischen 'Albanien' und 'Andorra' gesucht wird
    stat_bundesamt_laenge = stat_bundesamt[stat_bundesamt[spaltenname_laender] == 'Andorra'].index[0] - stat_bundesamt[stat_bundesamt[spaltenname_laender] == 'Albanien'].index[0]
    # selektiere die Zeilen mit Ländernamen und resette Index
    stat_bundesamt_laender = stat_bundesamt.iloc[range(0, len(stat_bundesamt),stat_bundesamt_laenge),:]
    stat_bundesamt_laender = stat_bundesamt_laender.reset_index()
    
    # ermittle Zeilennamen der Datenbank, z.B. 'Erwerbsquote', 'Selbstständigenquote', ...
    stat_bundesamt_zeilennamen = []
    for i in range(1, stat_bundesamt_laenge):
        stat_bundesamt_zeilennamen.append(stat_bundesamt.iloc[i,:][spaltenname_laender])
    
    # Liste mit den Datenbanken aus der Datei initialisieren
    stat_bundesamt_df = []
    # über Zeilennamen (Erwerbsquote, Selbständigenquote, ...) iterieren
    for zeilenname in stat_bundesamt_zeilennamen:
        # entsprechende Zeilen aus der großen Datenbank auswählen
        loop_df = stat_bundesamt[stat_bundesamt[spaltenname_laender] == zeilenname]
        # Index resetten, da sonst nächster Schritt nicht funktioniert
        loop_df = loop_df.reset_index()
        # Spalte mit Ländernamen hinzufügen
        loop_df['Land'] = stat_bundesamt_laender.iloc[range(len(loop_df)), :][spaltenname_laender]
        loop_df = loop_df.drop(['index', 'Einheit', spaltenname_laender], axis = 1)
        loop_df = set_land_index(loop_df, 'Land', laender_df)
        loop_df = loop_df.drop(['Land'], axis = 1)
        
        stat_bundesamt_df.append(loop_df)
    return stat_bundesamt_df

# Erwerbs(losen)quote und ähnliches einlesen
dateiname = 'Indizes/Erwerbsquote_international.xlsx'
spaltenname_laender = 'Gegenstand der Nachweisung'

[_, dict_dataframes['erwerbsquote'], dict_dataframes['selbstständigenquote'], dict_dataframes['erwerbslosenquote'], _, _, dict_dataframes['arbeitskosten']] = einlesen_stat_bundesamt(dateiname, spaltenname_laender)

# Internationale Wirtschaftsfaktoren einlesen
dateiname = 'Indizes/Wirtschaft_international.xlsx'
spaltenname_laender = 'Gegenstand der Nachweisung'
[_, dict_dataframes['bip_pers'], _, _, dict_dataframes['bws_land'], dict_dataframes['bws_prod'], dict_dataframes['bws_dienst']] = einlesen_stat_bundesamt(dateiname, spaltenname_laender)

# nicht mehr verwendete Variablen löschen
try:
    del dateiname, spaltenname_laender
except:
    pass

###----------------------------------------------------------------------###
### HDI einlesen
###----------------------------------------------------------------------### 

print('HDI wird eingelesen')

df_hdi = pd.read_csv('Indizes/Human development index (HDI).csv', encoding = 'ANSI', header = 1)
df_hdi = set_land_index(df_hdi, 'Country', laender_df)
df_hdi = df_hdi.drop(['HDI Rank (2018)', 'Country'], axis = 1)

# Variable speichern
dict_dataframes['hdi'] = df_hdi
try:
    del df_hdi
except:
    pass

###----------------------------------------------------------------------###
### Kriminalitätsstatistik einlesen
###----------------------------------------------------------------------### 

print('Kriminalitätsstatistik wird eingelesen')

# key: ------
dict_dataframes['krim_gesamt'] = pd.DataFrame(index = laender_df.index)
# key: 000000
dict_dataframes['krim_leben'] = pd.DataFrame(index = laender_df.index)
# key: 100000
dict_dataframes['krim_sex'] = pd.DataFrame(index = laender_df.index)
# key: 200000
dict_dataframes['krim_rohheit'] = pd.DataFrame(index = laender_df.index)
# key: 890000
dict_dataframes['krim_gesamt_ohne_aufenthalt'] = pd.DataFrame(index = laender_df.index)
# key: 892000
dict_dataframes['krim_gewalt'] = pd.DataFrame(index = laender_df.index)
# key: 893000
dict_dataframes['krim_wirtschaft'] = pd.DataFrame(index = laender_df.index)


for jahr in range(2012, 2019):
    # einlesen
    if jahr < 2014:
        dateiname = 'Indizes/Kriminalitätsstatistik/' + str(jahr) + '_tb62_TatverdaechtigeNichtdeutscheStraftatenStaatsangehoerigkeit_excel.xls'
    elif jahr < 2016:
        dateiname = 'Indizes/Kriminalitätsstatistik/' + str(jahr) + '_tb62_TatverdaechtigeNichtdeutscheStraftatenStaatsangehoerigkeit_excel.xlsx'
    else:
        dateiname = 'Indizes/Kriminalitätsstatistik/' + str(jahr) + '_STD-TV-16-T62-TV-Staatsangehoerigkeiten_excel.xlsx'
    krim_stat = pd.read_excel(dateiname, header = 4)
    # transponieren - pro Land eine Zeile
    krim_stat = krim_stat.T
    # ändere Namen der Zeilen, um mit den keys zugreifen zu können
    krim_stat.columns = krim_stat.iloc[0,:]
    # Setze Länderkürzel als Reihenindizes
    krim_stat['Land'] = krim_stat.index
    krim_stat = set_land_index(krim_stat, 'Land', laender_df)
    # nur die Zeilen verwenden, die ich später benutzen werde
    krim_stat = krim_stat[['------', '000000', '100000', '200000', '890000', '892000', '893000']]
    # mehrfache Ländereinträge addieren
    krim_stat = krim_stat.groupby(krim_stat.index).sum()
    
    dict_dataframes['krim_gesamt'][str(jahr)] = krim_stat['------']
    dict_dataframes['krim_leben'][str(jahr)] = krim_stat['000000']
    dict_dataframes['krim_sex'][str(jahr)] = krim_stat['100000']
    dict_dataframes['krim_rohheit'][str(jahr)] = krim_stat['200000']
    dict_dataframes['krim_gesamt_ohne_aufenthalt'][str(jahr)] = krim_stat['890000']
    dict_dataframes['krim_gewalt'][str(jahr)] = krim_stat['892000']
    dict_dataframes['krim_wirtschaft'][str(jahr)] = krim_stat['893000']

# nicht mehr verwendete Variablen löschen
try:
    del dateiname, jahr, krim_stat
except:
    pass
    
# gleiches für Opferzahlen
# key: ------
dict_dataframes['opf_stat_gesamt'] = pd.DataFrame(index = laender_df.index)


for jahr in range(2013, 2019):
    # einlesen
    if jahr < 2014:
        dateiname = 'Indizes/Kriminalitätsstatistik/' + str(jahr) + '_tb911_OpferNachStaatsangehoerigkeit_excel.xls'
    elif jahr < 2016:
        dateiname = 'Indizes/Kriminalitätsstatistik/' + str(jahr) + '_tb911_OpferNachStaatsangehoerigkeit_excel.xlsx'
    else:
        dateiname = 'Indizes/Kriminalitätsstatistik/' + str(jahr) + '_STD-O-02-T911-O-Staatsangehoerigkeiten_excel.xlsx'
    if jahr < 2015:
        opf_stat = pd.read_excel(dateiname, header = 3)
    else:
        opf_stat = pd.read_excel(dateiname, header = 4)
    # transponieren - pro Land eine Zeile
    opf_stat = opf_stat.T
    # ändere Namen der Zeilen, um mit den keys zugreifen zu können
    opf_stat.columns = opf_stat.iloc[0,:]
    # Setze Länderkürzel als Reihenindizes
    opf_stat['Land'] = opf_stat.index
    opf_stat = set_land_index(opf_stat, 'Land', laender_df)
    # nur die Zeilen verwenden, die ich später benutzen werde
    opf_stat = opf_stat.iloc[:, 0:3]
    # mehrfache Ländereinträge addieren
    opf_stat = opf_stat.groupby(opf_stat.index).sum()
    
    # in der Opferstatistik gibt es jeweils 3 Spalten. Wähle letzte Spalte (insgesamt)
    try:
        dict_dataframes['opf_stat_gesamt'][str(jahr)] = opf_stat['------'].iloc[:,-1]
    except:
        # in manchen Tabellen werden nicht alle Zeilen beschriftet. 
        # Die 3. Zeile müsste hier die richtige sein.
        #print(str(jahr))
        dict_dataframes['opf_stat_gesamt'][str(jahr)] = opf_stat.iloc[:,2]

# nicht mehr verwendete Variablen löschen
try:
    del opf_stat, jahr, dateiname
except:
    pass

###----------------------------------------------------------------------###
### Unabhängigkeitsdatum aus Wikipedia
###----------------------------------------------------------------------### 
import re
print('Unabhängigkeitsdatum aus Wikipedia wird eingelesen')

# Einlesen
df_unabhaengigkeit = pd.read_excel('Indizes/Laender_Unabhaengigkeitsdatum.xlsx')
# Länderkürzel als Index
df_unabhaengigkeit = set_land_index(df_unabhaengigkeit, 'Staat', laender_df)
# aus der Datumsspalte das Jahr als integer extrahieren
df_unabhaengigkeit['Jahr'] = [int(re.findall(r'\d{4}', str(datum))[0]) for datum in df_unabhaengigkeit['Datum']]
# in dataframe schreiben
dict_dataframes['unabhaengigkeit'] = df_unabhaengigkeit['Jahr']
try:
    del df_unabhaengigkeit
except:
    pass

###----------------------------------------------------------------------###
### Henley-Power-Index für Passports einlesen
###----------------------------------------------------------------------### 

print('Henley-Power-Index für Passports wird eingelesen')

# Eintrag im Dataframe initialisieren
dict_dataframes['passport_power'] = pd.DataFrame(index = laender_df.index)

# Schleife über verfügbare Jahre
for year in range(2006, 2021):
    
    # Tabelle einlesen (unformatiert)
    df_passport_power = pd.read_table('Indizes/Henley_passport_index/henley_passport_' + str(year) + '.txt',
                                      header = None, names = ['Land'])
    # Henley-Power-Index extrahieren
    df_passport_power[str(year)] = [int(re.findall(r'\d+', str(henley))[1]) for henley in df_passport_power['Land']]
    # aus der Länder-Spalte die Zahlen (Rang, Index) löschen
    df_passport_power['Land'] = [re.sub(r'\s\d+', '', land) for land in df_passport_power['Land']]
    # Länder-Code als Index setzen
    df_passport_power = set_land_index(df_passport_power, 'Land', laender_df)
    # Daten in dictionary schreiben
    dict_dataframes['passport_power'][str(year)] = df_passport_power[str(year)]


# temporäre Variablen löschen
try:
    del year, df_passport_power
except:
    pass

###----------------------------------------------------------------------###
### Europa-als-Ziel-Daten einlesen, insbesondere Regionen
###----------------------------------------------------------------------### 
    
print('Europa als Ziel-Daten (Regionen) werden eingelesen')

# Excel-Tabelle einlesen
df_europa = pd.read_excel('Indizes/Europa_als_Ziel.xlsx', header = 1)
# Ländernamen in Spalte 'Land' schreiben
df_europa['Land'] = df_europa.index
# Länder-Code als Index setzen
df_europa = set_land_index(df_europa, 'Land', laender_df)
#df_europa = df_europa.groupby(df_europa.index).max()

# einzelne Länder als dummie-Variablen
df_region_dummie = pd.get_dummies(df_europa['Region'])
dict_dataframes['Subsahara-Afrika'] = df_region_dummie['Subsahara-Afrika']
dict_dataframes['Mena'] = df_region_dummie['Mena']
dict_dataframes['Südasien'] = df_region_dummie['Südasien']
dict_dataframes['Ostasien'] = df_region_dummie['Ostasien']
dict_dataframes['Südostasien'] = df_region_dummie['Südostasien']
dict_dataframes['Postsowjetischer Raum'] = df_region_dummie['Postsowjetischer Raum']
dict_dataframes['Lateinamerika und Karibik'] = df_region_dummie['Lateinamerika und Karibik']

# Datenbank für Regionen
# ordne jeder Region einen Wert zu, um die Region in Intervallskalierte Variable zu transformieren
dict_regions = {'Subsahara-Afrika': 3,
                'Mena': 2,
                'Südasien': 2,
                'Ostasien': 2,
                'Südostasien': 2,
                'Postsowjetischer Raum': 1,
                'Lateinamerika und Karibik': 2}
# schreibe den entsprechenden Wert in die Datenbank
df_europa['Region'] = df_europa['Region'].map(dict_regions)

# Region in Datenbank schreiben
dict_dataframes['region'] = df_europa['Region']

try:
    del df_europa
except:
    pass


###----------------------------------------------------------------------###
### Clash of Civilizations als Rassismus-Index
###----------------------------------------------------------------------### 

print('Clash of Civilizations-Regionen werden eingelesen')

# Excel-Tabelle einlesen
df_civilization = pd.read_excel('Indizes/Clash_of_Civilizations.xlsx', header = 0)
# Länder-Code als Index setzen
df_civilization.index = df_civilization['Code']

# Civilization jeweils als einzelne dummy-Variablen
df_civilization_dummy = pd.get_dummies(df_civilization['Civilization'])

dict_dataframes['Western'] = df_civilization_dummy['Western']
dict_dataframes['Orthodox'] = df_civilization_dummy['Orthodox']
dict_dataframes['Islamic'] = df_civilization_dummy['Islamic']
dict_dataframes['Buddhist'] = df_civilization_dummy['Buddhist']
dict_dataframes['Hindu'] = df_civilization_dummy['Hindu']
dict_dataframes['African'] = df_civilization_dummy['African']
dict_dataframes['Latin American'] = df_civilization_dummy['Latin American']
dict_dataframes['Sinic'] = df_civilization_dummy['Sinic']
dict_dataframes['Japanese'] = df_civilization_dummy['Japanese']
dict_dataframes['Pacific Islander'] = df_civilization_dummy['Pacific Islander']

# Mappe Civilizations auf eine Ordinale Skala
mapper = {'Western': 1,
          'Orthodox': 1,
          #'Islamic': 4,
          'Islamic': 5,
          'Buddhist': 3,
          'Hindu': 3,
          'African':5,
          #'Latin American': 4,
          'Latin American': 3,
          #'Sinic': 2,
          #'Japanese': 2,
          'Sinic': 1,
          'Japanese': 1,
          'Pacific Islander': 3
          }
df_civilization['Civilization'] = df_civilization['Civilization'].replace(mapper)

dict_dataframes['civilization'] = df_civilization['Civilization']

try:
    del mapper, df_civilization, df_civilization_dummy
except:
    pass

###----------------------------------------------------------------------###
### UN-Liste ehemaliger Kolonien einlesen
###----------------------------------------------------------------------### 

print('UN-Liste ehemaliger Kolonien wird eingelesen')

df_non_self_governing = pd.read_excel('Indizes/former_non_self_governing_territories.xlsx')
df_non_self_governing = set_land_index(df_non_self_governing, 'Land', laender_df)

# dummy-Variable
# Eintrag im Dataframe initialisieren
dict_dataframes['non_self_governing'] = pd.DataFrame(index = laender_df.index)
# alle Werte zunächst auf 0 setzen
dict_dataframes['non_self_governing']['data'] = 0
dict_dataframes['non_self_governing']['data'][df_non_self_governing.index] = 1

# Eintrag für Jahr
# Eintrag im Dataframe initialisieren
dict_dataframes['non_self_governing_year'] = pd.DataFrame(index = laender_df.index)
# alle Werte zunächst auf 0 setzen
dict_dataframes['non_self_governing_year']['data'] = 1945
dict_dataframes['non_self_governing_year']['data'][df_non_self_governing.index] = df_non_self_governing['year']

try:
    del df_non_self_governing
except:
    pass


###----------------------------------------------------------------------###
### globaler Terrorismus Daten laden
###----------------------------------------------------------------------### 

print('Terrorismus-Daten werden eingelesen')

# Einträge im Dataframe initialisieren
dict_dataframes['terror_number'] = pd.DataFrame(index = laender_df.index)
dict_dataframes['terror_kill'] = pd.DataFrame(index = laender_df.index)
dict_dataframes['terror_wound'] = pd.DataFrame(index = laender_df.index)
   

# Terrorismus-Datenbank einlesen
df_terrorism = pd.read_excel('Indizes/globalterrorismdb_0919dist.xlsx', usecols = 'B, I, AA, CU, CX')
jahre = range(2000, 2020)
for jahr in jahre:
    # betrachte nur Anschläge aus einem Jahr
    df_terrorism_jahr = df_terrorism[ df_terrorism['iyear'] == jahr]
    # resette Index, damit set_land_index funktioniert
    df_terrorism_jahr = df_terrorism_jahr.reset_index(drop=True)
    # setze Länder-Code als Index
    df_terrorism_jahr = set_land_index(df_terrorism_jahr, 'country_txt', laender_df)
        # summiere Anzahl erfolgreicher Anschläge, Anzahl Toter und Anzahl verletzter
    df_terrorism_jahr = df_terrorism_jahr.groupby(df_terrorism_jahr.index)[['success', 'nkill', 'nwound']].sum()
    # ersetze nan durch 0
    df_terrorism_jahr = df_terrorism_jahr.fillna(0)
    # Daten in dictionary schreiben
    dict_dataframes['terror_number'][str(jahr)] = df_terrorism_jahr['success']
    dict_dataframes['terror_kill'][str(jahr)] = df_terrorism_jahr['nkill']
    dict_dataframes['terror_wound'][str(jahr)] = df_terrorism_jahr['nwound']
    # ersetze nan durch 0
    dict_dataframes['terror_number'][str(jahr)].fillna(0, inplace=True)
    dict_dataframes['terror_kill'][str(jahr)].fillna(0, inplace=True)
    dict_dataframes['terror_wound'][str(jahr)].fillna(0, inplace=True)

# temporäre Variablen löschen
try:
    del jahre, jahr, df_terrorism, df_terrorism_jahr
except:
    pass

###----------------------------------------------------------------------###
### State Power Index einlesen
###----------------------------------------------------------------------### 
    
print('State Power Index wird eingelesen')

# Excel-Tabelle einlesen
df_power = pd.read_excel('Indizes/State_Power_Index.xlsx')
# Länder-Code als Index
df_power = set_land_index(df_power, 'Country', laender_df)

# power-Wert und power-Rang in Dataframe speichern
dict_dataframes['power'] = df_power['State Power Index']
dict_dataframes['power_rank'] = df_power['Place']

try:
    del df_power
except:
    pass


###----------------------------------------------------------------------###
### Polity V - Index für Demokratie einlesen 
###----------------------------------------------------------------------### 
    
print('Polity V wird eingelesen')

# Excel-Tabelle einlesen
df_polity_v = pd.read_excel('Indizes/p5v2018.xls')
# Index in Ländercodes umändern
df_polity_v = set_land_index(df_polity_v, 'country', laender_df)

# Datenbanken initialisieren
# Achtung! für autoc und democ, da hier nicht nur Daten von 0 bis 10
# verwendet werden, sondern auch Sonderzahlen -66, -77, -88
dict_dataframes['polity_v_autoc'] = pd.DataFrame(index = df_polity_v.index.unique())
dict_dataframes['polity_v_democ'] = pd.DataFrame(index = df_polity_v.index.unique())
dict_dataframes['polity_v_polity'] = pd.DataFrame(index = df_polity_v.index.unique())

# Datenbank aufsplitten nach Jahren, 2005-2018
for year in range(2005, 2019):
    # Datenbanken für Daten aus polity-V-Index erstellen
    # in der gleichen Form wie bisher, pro Faktor eine Datenbank
    # in den Spalten jeweils die Daten pro Jahr
    #dict_dataframes['polity_v_autoc'][str(year)] = df_polity_v[df_polity_v['year'] == year]['autoc']
    #dict_dataframes['polity_v_democ'][str(year)] = df_polity_v[df_polity_v['year'] == year]['democ']
    
    # zunächst Zeile auslesen
    pol_year = df_polity_v[df_polity_v['year'] == year]['polity2']
    # in manchen Jahren tauchen Länder doppelt auf. Berechne hier Mittelwert
    pol_year = pol_year.groupby(pol_year.index).mean()
    # jetzt kann ins Dataframe geschrieben werden
    dict_dataframes['polity_v_polity'][str(year)] = pol_year
    # zunächst Zeile auslesen
    pol_year = df_polity_v[df_polity_v['year'] == year]['autoc']
    # setze negative Zahlen (-66, -77, -88 haben hier besondere Bedeutung) auf nan
    pol_year[pol_year < 0] = np.nan
    # in manchen Jahren tauchen Länder doppelt auf. Berechne hier Mittelwert
    pol_year = pol_year.groupby(pol_year.index).mean()
    # jetzt kann ins Dataframe geschrieben werden
    dict_dataframes['polity_v_autoc'][str(year)] = pol_year
    # zunächst Zeile auslesen
    pol_year = df_polity_v[df_polity_v['year'] == year]['democ']
    # setze negative Zahlen (-66, -77, -88 haben hier besondere Bedeutung) auf nan
    pol_year[pol_year < 0] = np.nan
    # in manchen Jahren tauchen Länder doppelt auf. Berechne hier Mittelwert
    pol_year = pol_year.groupby(pol_year.index).mean()
    # jetzt kann ins Dataframe geschrieben werden
    dict_dataframes['polity_v_democ'][str(year)] = pol_year


# nicht mehr verwendete Variablen löschen
try:
    del year, df_polity_v
except:
    pass

###----------------------------------------------------------------------###
### Freedom House Index einlesen 
###----------------------------------------------------------------------### 

print('Freedom House Index einlesen')

#Namen für die Spalten generieren, Name ist jeweils das Jahr
col_names = range(2000, 2018)
col_names = 3*list(col_names)
col_names.sort()
col_names = list(map(str, col_names))
#col_names[0::3] = [year + ' PR' for year in col_names[0::3]]
#col_names[1::3] = [year + ' CL' for year in col_names[1::3]]
#col_names[2::3] = [year + ' Status' for year in col_names[2::3]]
# erste Zeile sind Länder
col_names.insert(0, 'Country')

# Excel-Tabelle einlesen. Nur Daten ab 2000 mit entsprechend eben erstellten Namen
df_freedom_house = pd.read_excel('Indizes/Country and Territory Ratings and Statuses FIW1973-2018.xlsx',
                                'Country Ratings, Statuses ', usecols = "A, CE:EG",
                                names = col_names, skiprows=2)
# Länder-Codes als Index hinzufügen
df_freedom_house = set_land_index(df_freedom_house, 'Country', laender_df)
# Daten für PR einlesen und zu Zahlen konvertieren
df_freedom_house_PR = df_freedom_house.iloc[:, 1::3]
df_freedom_house_PR = df_freedom_house_PR.apply(pd.to_numeric, errors='coerce')
df_freedom_house_PR = df_freedom_house_PR.groupby(df_freedom_house_PR.index).mean()
# Daten für CL einlesen und zu Zahlen konvertieren
df_freedom_house_CL = df_freedom_house.iloc[:, 2::3]
df_freedom_house_CL = df_freedom_house_CL.apply(pd.to_numeric, errors='coerce')
df_freedom_house_CL = df_freedom_house_CL.groupby(df_freedom_house_CL.index).mean()

dict_dataframes['freedom_house_political'] = df_freedom_house_PR
dict_dataframes['freedom_house_civil'] = df_freedom_house_CL
dict_dataframes['freedom_house_both'] = df_freedom_house_CL + df_freedom_house_PR

try:
    del df_freedom_house, df_freedom_house_PR, df_freedom_house_CL
except:
    pass

###----------------------------------------------------------------------###
### World Bank GNI/capita ppp
###----------------------------------------------------------------------###
    
print('World Bank: GNI/capita einlesen')

#Excel-Datei einlesen
df_world_bank = pd.read_excel('Indizes/API_NY.GNP.PCAP.PP.CD_DS2_en_excel_v2_989283.xls', header=2)
# Länder-Code als Index
df_world_bank = set_land_index(df_world_bank, 'Country Code', laender_df)
# nicht benötigte Spalten löschen
df_world_bank = df_world_bank.drop(['Country Name', 'Country Code', 'Indicator Name', 'Indicator Code'], axis=1)
# Log_gni berechnen
df_world_bank = df_world_bank.apply(np.log)


# in Datenbank schreiben
dict_dataframes['log_gni_ppp'] = df_world_bank

###----------------------------------------------------------------------###
### fehlt noch: BTI_2006..., CPI2019..., hdro_statistical..., 
###----------------------------------------------------------------------### 








###----------------------------------------------------------------------###
### dataframe für die Regression zusammenbasteln
###----------------------------------------------------------------------### 

print('dataframe für die Regression wird erstellt')

# leere Zeilen löschen
#df_visa_abgelehnt = df_visa_abgelehnt.dropna(how = 'all')
#df_visa_erteilt = df_visa_erteilt.dropna(how = 'all')

# Daten bereinigen
for df in dict_dataframes:
    # Einträge in Zahlen umwandeln --> fehlende Daten zu nan
    dict_dataframes[df] = dict_dataframes[df].apply(pd.to_numeric, errors = 'coerce')
    # leere Zeilen löschen
    dict_dataframes[df] = dict_dataframes[df].dropna(how = 'all')
    
# Dictionary für die regression anlegen
dict_mult_reg = pd.DataFrame(index=laender_df.index)

# Liste mit Jahren, für die Visa-Daten vorliegen
years_available = set(dict_dataframes['visa_abgelehnt'].keys())

# Ablehnungsquote berechnen
visa_ablehnungsquote = dict_dataframes['visa_abgelehnt'].sum(axis=1) / (dict_dataframes['visa_erteilt'].sum(axis=1) + dict_dataframes['visa_abgelehnt'].sum(axis=1))
# leere Einträge löschen
visa_ablehnungsquote = visa_ablehnungsquote.dropna(how = 'all')

'''
print('Schengen-Länder werden eingelesen')
# schengen-Länder oder alle visabefreiten Länder rauslassen
#schengen = pd.read_excel('Laender_visafrei_2019.xlsx')
schengen = pd.read_excel('Laender_Schengen.xlsx')
schengen = set_land_index(schengen, 'Land', laender_df)
# um Error zu vermeiden, nur die Schengenländer auswählen, für die tatsächlich Daten vorliegen
schengen = set(schengen.index) & set(visa_ablehnungsquote.index)
# entsprechende Zeilen löschen
visa_ablehnungsquote = visa_ablehnungsquote.drop(schengen)
'''
# Visa-Ablehnungsquote ins Dataframe schreiben
dict_mult_reg['visa_ablehnungsquote'] = visa_ablehnungsquote



# Kolonialdaten
# das hier ist ein DataFrame, keine Series. Vielleicht macht das später Probleme

# Daten, die nicht gemittelt oder anders verarbeitet werden müssen
dict_mult_reg['col_deutsch'] = dict_dataframes['col_deutsch']
dict_mult_reg['col_ind_date'] = dict_dataframes['col_ind_date']
dict_mult_reg['unabhaengigkeit'] = dict_dataframes['unabhaengigkeit']
dict_mult_reg['region'] = dict_dataframes['region']
dict_mult_reg['civilization'] = dict_dataframes['civilization']
dict_mult_reg['Western'] = dict_dataframes['Western']
dict_mult_reg['Orthodox'] = dict_dataframes['Orthodox']
dict_mult_reg['Islamic'] = dict_dataframes['Islamic']
dict_mult_reg['Buddhist'] = dict_dataframes['Buddhist']
dict_mult_reg['Hindu'] = dict_dataframes['Hindu']
dict_mult_reg['African'] = dict_dataframes['African']
dict_mult_reg['Latin American'] = dict_dataframes['Latin American']
dict_mult_reg['Sinic'] = dict_dataframes['Sinic']
dict_mult_reg['Japanese'] = dict_dataframes['Japanese']
#drop Pacific Islander bereits hier, weil es da keine deutschen Botschaften gibt
#dict_mult_reg['Pacific Islander'] = dict_dataframes['Pacific Islander']
dict_mult_reg['Subsahara-Afrika'] = dict_dataframes['Subsahara-Afrika']
dict_mult_reg['Mena'] = dict_dataframes['Mena']
dict_mult_reg['Südasien'] = dict_dataframes['Südasien']
dict_mult_reg['Ostasien'] = dict_dataframes['Ostasien']
dict_mult_reg['Südostasien'] = dict_dataframes['Südostasien']
dict_mult_reg['Postsowjetischer Raum'] = dict_dataframes['Postsowjetischer Raum']
dict_mult_reg['Lateinamerika und Karibik'] = dict_dataframes['Lateinamerika und Karibik']

dict_mult_reg['non_self_governing'] = dict_dataframes['non_self_governing']
dict_mult_reg['non_self_governing_year'] = dict_dataframes['non_self_governing_year']
dict_mult_reg['power'] = dict_dataframes['power']
dict_mult_reg['power_rank'] = dict_dataframes['power_rank']


# Mittelwert für Spalten, für die mehrere Jahre vorliegen
for key in ['auslaender_gesamt', 'auslaender_mannlich', 'auslaender_weiblich', 
            'bip_pers', 'bws_dienst', 'bws_land', 'bws_prod', 'erwerbslosenquote', 
            'erwerbsquote', 'hdi', 'krim_gesamt_ohne_aufenthalt', 'krim_gewalt',
            'krim_rohheit', 'krim_sex', 'krim_gesamt', 'krim_leben',
            'krim_wirtschaft', 'opf_stat_gesamt', 'schutzsuchende_gesamt',
            'schutzsuchende_maennlich', 'schutzsuchende_weiblich',
            'world_happiness_confidence_nat_gover', 'world_happiness_deliv_quality',
            'world_happiness_dem_quality', 'world_happiness_freedom_choices',
            'world_happiness_generosity', 'world_happiness_life_expectancy',
            'world_happiness_life_ladder', 'world_happiness_log_gdp_capita',
            'world_happiness_perc_corruption', 'world_happiness_social_support', 'passport_power',
            'terror_number', 'terror_kill', 'terror_wound', 'polity_v_polity',
            'polity_v_democ', 'polity_v_autoc', 'freedom_house_civil',
            'freedom_house_political', 'freedom_house_both', 'log_gni_ppp']:
    # Suche Jahre, für die Visadaten sowie Daten für den jeweiligen Datensatz vorliegen
    years = set(dict_dataframes[key].keys()) & years_available
    #years = set(map(str, dict_dataframes[key].keys())) & years_available
    years = list(years)
    # Schreibe Mittelwert in das Dictionary für die Regression
    dict_mult_reg[key] = dict_dataframes[key][years].mean(axis = 1)
    
# relative Kriminalität berechnen
for key in ['krim_gesamt_ohne_aufenthalt', 'krim_gewalt',
            'krim_rohheit', 'krim_sex', 'krim_gesamt', 'krim_leben',
            'krim_wirtschaft', 'opf_stat_gesamt']:
    # Schreibe krim / auslaender_gesamt ins Dictionary
    dict_mult_reg['rel_' + key] = dict_mult_reg[key] / dict_mult_reg['auslaender_gesamt']
 
# erstelle neuen Faktor: schutzsuchende_rel
dict_mult_reg['schutzsuchende_rel'] = dict_mult_reg['schutzsuchende_gesamt']/dict_mult_reg['auslaender_gesamt']
# neuer Faktor: 'erwerb', indem erwerbslosen- und erwerbsquote verrechnet sind
dict_mult_reg['erwerb'] = dict_mult_reg['erwerbsquote'] - dict_mult_reg['erwerbslosenquote']
dict_mult_reg['erwerb_quotient'] = dict_mult_reg['erwerbsquote'] / dict_mult_reg['erwerbslosenquote']
dict_mult_reg['erwerb_stand'] = standardize(dict_mult_reg, 'erwerbsquote') - standardize(dict_mult_reg, 'erwerbslosenquote')

# neuer Faktor für Postkolonialismus: power gewichtet mit non-self-governing
dict_mult_reg['postkol'] = dict_mult_reg['power'] / (dict_mult_reg['non_self_governing'] + 1)





###----------------------------------------------------------------------###
### Daten in CSV abspeichern
###----------------------------------------------------------------------### 
print('Daten werden abgespeichert')

dict_mult_reg.to_csv('daten_mult_reg.csv')

###----------------------------------------------------------------------###
### Variante mit standardisierten Variablen
###----------------------------------------------------------------------### 

print('Daten werden standardisiert')


dict_mult_reg_stand = dict_mult_reg.copy(deep=True)

for key in dict_mult_reg_stand:
    if not key == 'visa_ablehnungsquote':
        dict_mult_reg_stand[key] = standardize(dict_mult_reg, key)
    
dict_mult_reg_stand.to_csv('daten_mult_reg_stand.csv')

# keys_visa: zu addierende Spalten
# dummy = dict_dataframes['visa_abgelehnt']
# dummy_2 = dummy[['2007', '2008']].sum(axis=1)
# dummy_2 = dummy[list(set(['2007', '2008']))].mean(axis=1)
# dummy['mean'] = dummy[['2007', '2008']].mean(axis=1)


# Todo vor der Mult-Reg:
# Schengen-Länder streichen! am besten in Zeile 523


###----------------------------------------------------------------------###
### Visafreie Länder rauslassen
###----------------------------------------------------------------------### 

print('Visafreie Länder werden eingelesen')
# schengen-Länder oder alle visabefreiten Länder rauslassen
visafrei = pd.read_excel('Laender_visafrei_2019.xlsx')
visafrei = set_land_index(visafrei, 'Land', laender_df)
# um Error zu vermeiden, nur die visafreien auswählen, für die tatsächlich Daten vorliegen
visafrei = set(visafrei.index) & set(dict_mult_reg.index)
# entsprechende Zeilen löschen
dict_mult_reg = dict_mult_reg.drop(visafrei)
dict_mult_reg_stand = dict_mult_reg_stand.drop(visafrei)

# jetzt nochmal als csv speichern
print('Daten werden nochmal abgespeichert')

dict_mult_reg.to_csv('daten_mult_reg_visafrei.csv')
dict_mult_reg_stand.to_csv('daten_mult_reg_visafrei_stand.csv')