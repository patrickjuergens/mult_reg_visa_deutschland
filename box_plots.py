# -*- coding: utf-8 -*-


# importiere benötigte Module
import numpy as np
import pandas as pd
import seaborn as sns
from scipy import stats
import matplotlib.pyplot as plt

import statsmodels.api as sm
from statsmodels.stats import diagnostic as diag
from statsmodels.stats.outliers_influence import variance_inflation_factor

from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error

import read_country



###----------------------------------------------------------------------###
### Load the Data into Pandas
###----------------------------------------------------------------------###

# Daten einlesen
# zur Reproduktion der Ergebnisse der Bachelorarbeit, verwende als Quelle
# 'verwendete_daten.csv'
visa_df = pd.read_csv('daten_mult_reg.csv')
# Länderspalte als Index setzen
visa_df = visa_df.set_index('Code')
# gehe sicher, das alle Elemente Zahlen sind
visa_df = visa_df.astype(float)
# Lasse Länder weg, in denen keine Auslandsvertretung besteht
visa_df = visa_df[visa_df['visa_ablehnungsquote'].notna()]


# Lade die Faktoren, die weggelassen werden sollen
list_to_drop = np.load('list_to_drop.npy')
# einige Faktoren doch behalten
list_to_drop = list(list_to_drop)
list_not_drop = ['Subsahara-Afrika', 'Südasien', 'Ostasien', 'Südostasien', 
                'Mena', 'Lateinamerika und Karibik', 'Postsowjetischer Raum']
for element in list_not_drop:
    try:
        list_to_drop.remove(element)
    except:
        pass
# droppe die Faktoren
visa_df = visa_df.drop(list_to_drop, axis = 1)
# lasse alle Länder mit fehlenden Daten weg
visa_df = visa_df.dropna()

#visa_df.to_csv('verwendete_daten.csv')

# manche Daten Transformieren
# power Logarithmieren
visa_df['log_power'] = visa_df['power'].apply(np.log)
# Ablehnungsquote wurzeln, da die Quote quasi poissonverteilt ist
visa_df['sqrt_visa_ablehnungsquote'] = visa_df['visa_ablehnungsquote'].apply(lambda x: x**0.5)
visa_df['sqrt_schutzsuchende_rel'] = visa_df['schutzsuchende_rel'].apply(lambda x: x**0.5)
visa_df['sqrt_rel_krim_gesamt_ohne_aufenthalt'] = visa_df['rel_krim_gesamt_ohne_aufenthalt'].apply(lambda x: x**0.5)



# manche Daten Transformieren
# power Logarithmieren
visa_df['log_terror_kill'] = visa_df['terror_kill'].apply(lambda x: np.log(x+1))
visa_df['log_col_ind_year'] = visa_df['col_ind_date'].apply(lambda x: np.log(2020-x))
visa_df['col_ind_year'] = visa_df['col_ind_date'].apply(lambda x: 2020-x)
visa_df['log_visa_ablehnungsquote'] = visa_df['visa_ablehnungsquote'].apply(np.log)
visa_df['log_schutzsuchende_rel'] = visa_df['schutzsuchende_rel'].apply(np.log)
visa_df['log_rel_krim_gesamt_ohne_aufenthalt'] = visa_df['rel_krim_gesamt_ohne_aufenthalt'].apply(np.log)

# Schengen-Länder und visafreie Länder einlesen
print('Schengen-Länder werden eingelesen')

# Schengen-Länder
schengen = pd.read_excel('Laender_Schengen.xlsx')
schengen = read_country.set_land_index(schengen, 'Land')
# um Error zu vermeiden, nur die Schengenländer auswählen, für die tatsächlich Daten vorliegen
schengen = set(schengen.index) & set(visa_df.index)
# entsprechende Zeilen als Dataframe lesen
schengen_df = visa_df.loc[schengen]

# visafreie Länder
visafrei = pd.read_excel('Laender_visafrei_2019.xlsx')
visafrei = read_country.set_land_index(visafrei, 'Land')
# die Länder, für die Auslandsvertretungen bestehen und die keine Schengen-Länder sind
visafrei = (set(visafrei.index) & set(visa_df.index) ) - schengen
# entsprechende Zeilen als Dataframe
visafrei_df = visa_df.loc[visafrei]

# Dataframe für alle anderen Länder
visapflichtig_df = visa_df.drop(visafrei).drop(schengen)
    
for key in visa_df.keys():
    if key not in ('Subsahara-Afrika', 'non_self_governing', 'Südasien', 
                   'Ostasien', 'Südostasien', 'Mena', 
                   'Lateinamerika und Karibik', 'Postsowjetischer Raum'):
        # zu plottende Daten
        data = [visapflichtig_df[key], visafrei_df[key], schengen_df[key]]
        # Plot initialisieren
        fig, ax = plt.subplots()
        # Plot erstellen
        ax.boxplot(data)
        # Plots beschriften
        ax.set_xticklabels(['visapflichtig', 'visafrei', 'Schengen'])
        

    else:
        # zu plottende Daten
        data = [visapflichtig_df[key].sum()/len(visapflichtig_df[key]), 
                visafrei_df[key].sum()/len(visafrei_df[key]), 
                schengen_df[key].sum()/len(schengen_df[key])]
        # Plots beschriften
        labels = ['visapflichtig', 'visafrei', 'Schengen']
        # Plot initialisieren
        fig = plt.figure()
        ax = fig.add_axes([0,0,1,1])
        # Plot erstellen
        ax.bar(labels, data)
    
    # Titel für die Plots
    ax.set_title(key)
    
    plt.show()
    
    fig.savefig('figures/' + key + '.png', bbox_inches='tight')

    # extra Plot für Terror ohne die Outliers
    if key == 'terror_kill':
        # zu plottende Daten
        data = [visapflichtig_df[key], visafrei_df[key], schengen_df[key]]
        # Plot initialisieren
        fig, ax = plt.subplots()
        # Plot erstellen
        ax.boxplot(data)
        # Plots beschriften
        ax.set_xticklabels(['visapflichtig', 'visafrei', 'Schengen'])
        # Limit von Hand setzen, damit überhaupt etwas sichtbar wird
        plt.ylim((-10, 250))
        
        # Titel für die Plots
        ax.set_title(key)
        plt.show()
    
        fig.savefig('figures/' + key + '_lim.png', bbox_inches='tight')
    elif key == 'power':
        # zu plottende Daten
        data = [visapflichtig_df[key], visafrei_df[key], schengen_df[key]]
        # Plot initialisieren
        fig, ax = plt.subplots()
        # Plot erstellen
        ax.boxplot(data)
        # Plots beschriften
        ax.set_xticklabels(['visapflichtig', 'visafrei', 'Schengen'])
        # Limit von Hand setzen, damit überhaupt etwas sichtbar wird
        plt.ylim((0, 2))
        
        # Titel für die Plots
        ax.set_title(key)
        plt.show()
    
        fig.savefig('figures/' + key + '_lim.png', bbox_inches='tight')
      
        
# get the summary in numbers
# visapflichtig
desc_df_visapflicht = visapflichtig_df.describe()

# add the standard deviation metric
desc_df_visapflicht.loc['+3_std'] = desc_df_visapflicht.loc['mean'] + (desc_df_visapflicht.loc['std'] * 3)
desc_df_visapflicht.loc['-3_std'] = desc_df_visapflicht.loc['mean'] - (desc_df_visapflicht.loc['std'] * 3)

# visafrei
desc_df_visafrei = visafrei_df.describe()

# add the standard deviation metric
desc_df_visafrei.loc['+3_std'] = desc_df_visafrei.loc['mean'] + (desc_df_visafrei.loc['std'] * 3)
desc_df_visafrei.loc['-3_std'] = desc_df_visafrei.loc['mean'] - (desc_df_visafrei.loc['std'] * 3)

# Schengen
desc_df_schengen = schengen_df.describe()

# add the standard deviation metric
desc_df_schengen.loc['+3_std'] = desc_df_schengen.loc['mean'] + (desc_df_schengen.loc['std'] * 3)
desc_df_schengen.loc['-3_std'] = desc_df_schengen.loc['mean'] - (desc_df_schengen.loc['std'] * 3)
